﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mooove
{
    public class FileOperationsHandler
    {
        /// <summary>
        /// TODO TESTS!
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="source"></param>
        /// <param name="sourceFileName"></param>
        /// <param name="destination"></param>
        /// <returns></returns>        
        public Tuple<bool, string> Process(FileSystemOperations operation, string source, string destination)
        {
            if (string.IsNullOrWhiteSpace(source) || string.IsNullOrWhiteSpace(destination) || !Directory.Exists(Path.GetDirectoryName(source)))
                return new Tuple<bool, string>(false, "Source or destination blank");

            var sourceType = File.Exists(source) ? SourceType.File : SourceType.Directory;

            try {
                // DeleteDestination(sourceType, source, destination); // NO!
                //CreateDestination(destination);
                return sourceType == SourceType.File ?
                    PerformFileAction(operation, source, destination) :
                    PerformDirectoryAction(operation, source, destination);
            }
            catch (Exception e) {
                return new Tuple<bool, string>(false, e.Message);
            }
        }

        public Tuple<bool, string> CreateDestination(string destination)
        {
            if (!File.Exists(destination) || Directory.Exists(destination)) return null;
            if (!Directory.Exists(destination)) {
                Directory.CreateDirectory(destination);
            }
            return new Tuple<bool, string>(true, "Ok");
        }

        public Tuple<bool, string> DeleteDestination(SourceType sourceType, string source, string destination)
        {
            if (sourceType == SourceType.File) {
                var target = Path.Combine(destination, Path.GetFileName(source));
                if (File.Exists(target)) {
                    File.Delete(target);
                }
            }
            else {
                var target = Path.Combine(destination, Path.GetDirectoryName(source));
                if (Directory.Exists(target)) {
                    Directory.Delete(target, true);
                }
            }
            return new Tuple<bool, string>(true, "Successful");
        }

        public Tuple<bool, string> PerformFileAction(FileSystemOperations operation, string source, string destination)
        {
            if (string.IsNullOrWhiteSpace(source) || string.IsNullOrWhiteSpace(destination) || string.IsNullOrWhiteSpace(Path.GetFileName(source)))
                return new Tuple<bool, string>(false, "Invalid parameters");

            // is the destination a file? go up the tree to the directory
            if (File.Exists(destination) && Directory.GetParent(destination).Exists)
                destination = Directory.GetParent(destination).FullName;

            // if no destination directory found, create it
            if (!Directory.Exists(destination))
                CreateDestination(destination);

            // if file exists, delete it!
            if (File.Exists(destination))
                DeleteDestination(SourceType.File, source, Path.Combine(destination, Path.GetFileName(source)));

            // always overwrite destination
            File.Copy(source, Path.Combine(destination, Path.GetFileName(source)), true);

            // delete original file for a move operation
            if (operation == FileSystemOperations.Move)
                File.Delete(source);
            
            // return success
            return new Tuple<bool, string>(true, "Successful");
        }

        public Tuple<bool, string> PerformDirectoryAction(FileSystemOperations operation, string source, string destination)
        {
            if (string.IsNullOrWhiteSpace(source) || string.IsNullOrWhiteSpace(destination))
                return new Tuple<bool, string>(false, "Paths invalid");

            if (!source.EndsWith("\\")) source += "\\";
            if (!destination.EndsWith("\\")) destination += "\\";

            DirectoryInfo dir = new DirectoryInfo(Path.GetDirectoryName(source));
            if (!dir.Exists)
                return new Tuple<bool, string>(false, "Source directory does not exist or could not be found");            

            // Get the subdirectories for the specified directory.
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destination))
                Directory.CreateDirectory(destination);
            
            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files) {
                string temppath = Path.Combine(destination, file.Name);
                file.CopyTo(temppath, true);
            }

            // If copying subdirectories, copy them and their contents to new location.
            // todo, create the destination subdirectories
            foreach (DirectoryInfo subdir in dirs) {
                string temppath = Path.Combine(destination, subdir.Name);
                PerformDirectoryAction(operation, subdir.FullName, temppath);
            }

            if (operation == FileSystemOperations.Move)
                Directory.Delete(source, true);

            return new Tuple<bool, string>(true, "Sucessful");
        }
    }

    public enum FileSystemOperations
    {
        Copy,
        Move
    }

    public enum SourceType
    {
        File,
        Directory
    }
}