﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Mooove
{
    public class Favourite : IEquatable<Favourite>
    {
        public string Path { get; set; }
        public string PathLowercase => Path.ToLowerInvariant();
        public int Uses { get; set; }
        public Visibility ImageVisibility { get; set; }

        public bool Equals(Favourite other)
        {
            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(this, other)) return true;
            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(this, null) || Object.ReferenceEquals(other, null))
                return false;
            //Check whether the products' properties are equal.
            return this.Path == other.Path;
        }

        public override int GetHashCode()
        {
            //Get hash code for the Name field if it is not null. 
            int hashProductName = Path == null ? 0 : Path.GetHashCode();
            //Get hash code for the Code field. 
            int hashProductCode = ImageVisibility.GetHashCode();
            //Calculate the hash code for the product. 
            return hashProductName ^ hashProductCode;
        }
    }
}
