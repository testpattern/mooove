﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mooove
{
    public class AutocompleteHandler
    {
        public IList<string> GetDirectories(string path)
        {
            var result = new List<string>();

            try {
                result = Directory.GetDirectories(path, "*.*", SearchOption.TopDirectoryOnly).ToList();
                return result;
            }
            catch (Exception ex) {
                return new List<string> { ex.Message };
            }
        }

        public IList<string> GetFiles(string path)
        {
            var result = new List<string>();

            try {
                result = Directory.GetDirectories(path, "*.*", SearchOption.TopDirectoryOnly).ToList();
                var files = Directory.GetFiles(path);
                result.AddRange(files);
                return result;
            }
            catch (Exception ex) {
                return new List<string> { ex.Message };
            }
        }
    }
}
