﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Mooove
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Favourite> Favourites { get; set; }
        public string Source;

        private Regex _validPathRegex = new Regex(@"[a-zA-Z]:\\");
        private readonly FileOperationsHandler _fileOperationHandler;
        private readonly AutocompleteHandler _autocompleteHandler;
        private readonly FavouritesRepository _favouritesRepository;
        private readonly FavouritesHandler _favouritesHandler;

        public MainWindow()
        {
            InitializeComponent();

            if (!Environment.CommandLine.Contains("vshost"))
                if (Environment.GetCommandLineArgs()[1] == null || !Environment.GetCommandLineArgs()[1].Contains("\\"))
                    this.Close();

            _fileOperationHandler = new FileOperationsHandler();
            _favouritesRepository = new FavouritesRepository(ConfigurationHandler.FavouritesFilename, Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            _favouritesHandler = new FavouritesHandler(_favouritesRepository);
            _autocompleteHandler = new AutocompleteHandler();
            Source = Environment.GetCommandLineArgs().Length > 1 ? Environment.GetCommandLineArgs()[1] : "";
            Sources.Text = Source;

            Favourites = new ObservableCollection<Favourite>(_favouritesRepository.GetAll().Take(10));
            FavouritesList.ItemsSource = Favourites;
        }

        private void Sources_Populating(object sender, PopulatingEventArgs e)
        {
            var text = Sources.Text;

            if (!_validPathRegex.IsMatch(text)) return;

            if (File.Exists(text)) // it's a file, don't need to look up anything
                return;

            var directory = Path.GetDirectoryName(text);
            if (Directory.Exists(directory)) {
                Sources.ItemsSource = _autocompleteHandler.GetFiles(directory);
                Sources.PopulateComplete();
            }
        }

        private void Destinations_Populating(object sender, PopulatingEventArgs e)
        {
            var text = Destinations.Text;

            if (!_validPathRegex.IsMatch(text)) return;

            var directory = Path.GetDirectoryName(text);

            if (Directory.Exists(directory)) {
                Destinations.ItemsSource = _autocompleteHandler.GetDirectories(directory);
                Destinations.PopulateComplete();
            }
        }

        private void Pin_Click(object sender, RoutedEventArgs e)
        {
            if (BlankDestination()) return;
            var result = _favouritesHandler.Pin(Destinations.Text);
            if (result.Item1) {
                Favourites.Add(result.Item2);
                Favourites.Remove(_favouritesRepository.Default().First());
            }
            else {
                UpdateStatusText(result.Item2.Path, true);
            }
        }

        private void Unpin_Click(object sender, RoutedEventArgs e)
        {
            var favouritePath = ((TextBlock)VisualTreeHelper.GetChild(VisualTreeHelper.GetChild(VisualTreeHelper.GetParent(((Button)sender).Parent), 0), 0)).Text;
            var result = _favouritesHandler.Unpin(favouritePath);
            if (result.Item1) {
                Favourites.Remove(result.Item2);
                if (!FavouritesList.HasItems)
                    Favourites.Add(_favouritesRepository.Default().First());
            }
            else {
                UpdateStatusText(result.Item2.Path, true);
            }
        }

        private void FavouritesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = FavouritesList?.SelectedItem as Favourite;
            if (selected == null || selected.ImageVisibility == Visibility.Collapsed) return;
            Destinations.Text = selected.Path;
        }

        private void Move_Click(object sender, RoutedEventArgs e)
        {
            if (BlankDestination() || BlankSource()) return;
            ConfirmOperation(((Button)sender).Name);
        }

        private void Copy_Click(object sender, RoutedEventArgs e)
        {
            if (BlankDestination() || BlankSource()) return;
            ConfirmOperation(((Button)sender).Name);
        }

        private void ConfirmOperation(string sender)
        {
            var destinationValue = !string.IsNullOrWhiteSpace(Destinations.Text) ?
                Destinations.Text : Destinations.SelectedItem?.ToString();
            var sourceValue = !string.IsNullOrWhiteSpace(Sources.Text) ?
                Sources.Text : Sources.SelectedItem?.ToString();

            if (string.IsNullOrWhiteSpace(destinationValue) || string.IsNullOrWhiteSpace(sourceValue)) {
                UpdateStatusText("Invalid values", true);
                return;
            }

            var operation = (FileSystemOperations)Enum.Parse(typeof(FileSystemOperations), sender);
            var destination = destinationValue.TrimEnd('\\');
            var source = sourceValue.TrimEnd('\\');
            var sourceFilename = sourceValue.Substring(source.LastIndexOf('\\')).Trim('\\');

            var result = new Tuple<bool, string>(false, "Unsuccessful");
            var favouriteItem = _favouritesRepository.GetFromPath(destination);
            if (favouriteItem != null)
                _favouritesRepository.UpdateItem(favouriteItem);

            try {
                result = _fileOperationHandler.Process(operation, source, destination);
                UpdateStatusText(result.Item2);
            }
            catch (Exception ex) {
                UpdateStatusText($"An error occurred: {ex.Message}", true);
            }
            // todo, progress bar? some sort of acknowledgement that something is happening
            //       especially because a large file will take a 
            // todo, decide what happens when the action completes
            if (operation == FileSystemOperations.Move) Sources.Text = "";
        }

        private bool BlankSource()
        {
            var emptySource = string.IsNullOrWhiteSpace(Sources.Text);
            Sources.Background = emptySource ? Brushes.MistyRose : Brushes.White;
            Sources.BorderBrush = emptySource ? Brushes.Red : Brushes.Black;
            return emptySource;
        }

        private bool BlankDestination()
        {
            var emptyDestination = string.IsNullOrWhiteSpace(Destinations.Text);
            Destinations.Background = emptyDestination ? Brushes.MistyRose : Brushes.White;
            Destinations.BorderBrush = emptyDestination ? Brushes.Red : Brushes.Black;
            return emptyDestination;
        }

        private void UpdateStatusText(string text, bool isError = false)
        {
            StatusText.Foreground = isError ? Brushes.Red : Brushes.Black;
            StatusText.Text = text;
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.OemQuestion) && Keyboard.IsKeyDown(Key.LeftShift)) {
                var message = "SHIFT+C = Copy\nSHIFT+X = Move\nSHIFT+P = Pin\nSHIFT+U = Unpin\n";
                MessageBox.Show(message, "Keyboard shortcuts", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            if (Keyboard.IsKeyDown(Key.LeftShift) && Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.C)) {
                if (BlankDestination() || BlankSource()) return;
                ConfirmOperation("Copy");
            }
            if (Keyboard.IsKeyDown(Key.LeftShift) && Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.X)) {
                if (BlankDestination() || BlankSource()) return;
                ConfirmOperation("Move");
            }
        }
    }
}
