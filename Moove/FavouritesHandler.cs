﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mooove
{
    public class FavouritesHandler
    {
        private readonly FavouritesRepository _favouritesRepository;

        public FavouritesHandler(FavouritesRepository _favRepo)
        {
            _favouritesRepository = _favRepo;
        }

        public Tuple<bool, Favourite> Pin(string favouritePath)
        {
            if (string.IsNullOrWhiteSpace(favouritePath))
                return new Tuple<bool, Favourite>(false, new Favourite { Path = "Invalid value" });

            var newItem = _favouritesRepository.BuildFromPath(favouritePath);
            var result = _favouritesRepository.AddItem(newItem);

            return result.Item1 ?
                new Tuple<bool, Favourite>(result.Item1, newItem) :
                new Tuple<bool, Favourite>(result.Item1, new Favourite { Path = result.Item2 });
        }

        public Tuple<bool, Favourite> Unpin(string favouritePath)
        {
            if (string.IsNullOrWhiteSpace(favouritePath))
                return new Tuple<bool, Favourite>(false, new Favourite { Path = "Invalid value" });

            var removeItem = _favouritesRepository.BuildFromPath(favouritePath);
            var result = _favouritesRepository.RemoveItem(removeItem);

            return result.Item1 ?
                new Tuple<bool, Favourite>(result.Item1, removeItem) :
                new Tuple<bool, Favourite>(result.Item1, new Favourite { Path = result.Item2 });
        }
    }
}
