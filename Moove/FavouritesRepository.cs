﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mooove
{
    public class FavouritesRepository
    {
        public string CombinedPath => Path.Combine(_path, _filename);
        private static string _filename;
        private static string _path;

        public FavouritesRepository(string filename, string path)
        {
            _filename = filename;
            _path = path;
        }

        public List<Favourite> GetAll()
        {
            if (!File.Exists(CombinedPath)) return Default();

            var file = new FileStream(CombinedPath, FileMode.Open, FileAccess.Read);
            if (file == null) return Default();

            var result = new List<Favourite>();

            using (var reader = new StreamReader(file)) {
                while (!reader.EndOfStream) {
                    var line = reader.ReadLine();
                    if (!string.IsNullOrWhiteSpace(line))
                        result.Add(BuildFromLine(line));
                }
            }

            return result.Any() ? result.Distinct().OrderByDescending(item => item.Uses).ThenBy(item => item.Path).ToList() : Default();
        }

        public Favourite GetFromPath(string path)
        {
            if (string.IsNullOrWhiteSpace(path)) return null;
            return GetAll().FirstOrDefault(item => item.Path.Equals(path, StringComparison.OrdinalIgnoreCase));
        }

        public Tuple<bool, string> UpdateItem(Favourite item)
        {
            if (item == null)
                return new Tuple<bool, string>(false, "Item is invalid");
            item.Uses++;
            if (string.IsNullOrWhiteSpace(item.Path))
                return new Tuple<bool, string>(false, "Path is not valid");
            if (!File.Exists(CombinedPath))
                return WriteNewFile(item);
            var content = File.ReadAllLines(CombinedPath);
            if (!content.Any())
                return new Tuple<bool, string>(false, "Favourites file empty");
            var result = content.Where(line => !line.Split(',').FirstOrDefault().Equals(item.Path, StringComparison.OrdinalIgnoreCase)).ToList();
            result.Add($"{item.Path},{item.Uses}");
            File.WriteAllLines(CombinedPath, result);
            return new Tuple<bool, string>(true, "Updated!");
        }

        public Tuple<bool, string> AddItem(Favourite item)
        {
            if (string.IsNullOrWhiteSpace(item.Path) || !Directory.Exists(item.Path))
                return new Tuple<bool, string>(false, "Path is not valid");
            if (!File.Exists(CombinedPath))
                return WriteNewFile(item);
            var content = File.ReadAllLines(CombinedPath);
            if (content.Any(line => line.ToLowerInvariant().StartsWith(item.PathLowercase)))
                return new Tuple<bool, string>(false, "Path is a duplicate entry");
            File.AppendAllLines(CombinedPath, new List<string> { $"{item.Path},{item.Uses}" });
            return new Tuple<bool, string>(true, "Added!");
        }

        public Tuple<bool, string> RemoveItem(Favourite item)
        {
            if (string.IsNullOrWhiteSpace(item.Path))
                return new Tuple<bool, string>(false, "Path is not valid");
            if (!File.Exists(CombinedPath))
                return new Tuple<bool, string>(false, "Favourites file not found");
            var content = File.ReadAllLines(CombinedPath);
            if (!content.Any())
                return new Tuple<bool, string>(false, "Favourites file empty");
            if (!content.Any(line => line.ToLowerInvariant().StartsWith(item.PathLowercase)))
                return new Tuple<bool, string>(true, "Path removed successfully");
            var newFileData = content.Where(line => !line.Split(',').FirstOrDefault().Equals(item.Path, StringComparison.OrdinalIgnoreCase));
            File.WriteAllLines(CombinedPath, newFileData);
            return new Tuple<bool, string>(true, "Removed!");
        }

        public Favourite BuildFromPath(string path)
        {
            return new Favourite { Path = path, Uses = 0, ImageVisibility = System.Windows.Visibility.Visible };
        }

        public List<Favourite> Default()
        {
            return new List<Favourite> {
                new Favourite {
                    Path = "No favourites found! Try adding destinations you commonly use (press that plus icon)",
                    ImageVisibility = System.Windows.Visibility.Collapsed
                }
            };
        }

        private Tuple<bool, string> WriteNewFile(Favourite item)
        {
            try {
                var fileStream = File.Create(CombinedPath);
                using (var writer = new StreamWriter(fileStream)) {
                    writer.WriteLine($"{item.Path},{item.Uses}");
                }
                return new Tuple<bool, string>(true, "Virgin entry!");
            }
            catch {
                return new Tuple<bool, string>(false, "An error occurred writing the file");
            }
        }

        Favourite BuildFromLine(string line)
        {
            var parts = line.Split(',');
            var path = parts[0];
            var uses = int.Parse(parts[1]);
            return new Favourite { Path = path, Uses = uses, ImageVisibility = System.Windows.Visibility.Visible };
        }

    }
}
