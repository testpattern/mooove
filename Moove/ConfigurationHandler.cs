﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mooove
{
    internal static class ConfigurationHandler
    {
        internal static string FavouritesFilename => System.Diagnostics.Debugger.IsAttached ?
            ConfigurationManager.AppSettings["FavouritesFilename.Debug"] :
            ConfigurationManager.AppSettings["FavouritesFilename.Live"];
    }
}
