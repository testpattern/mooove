﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mooove;
using System.IO;
using NUnit.Framework;

namespace Moove.Tests
{
    [TestClass]
    public class FileOperationsHandlerOperationsTests
    {
        private FileOperationsHandler subject = new FileOperationsHandler();

        [TestMethod]
        public void Move_file()
        {
            var source = @"d:\dev\moove\test-files\source\some1.txt";
            // ensure the file is there to be moved
            if (!File.Exists(source)) {
                subject.PerformFileAction(FileSystemOperations.Copy, @"d:\dev\moove\test-files\files\some1.txt", source);
            }
            var destination = @"d:\dev\moove\test-files\target\";
            var result = subject.Process(FileSystemOperations.Move, source, destination);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue(result.Item1);
        }
        [TestMethod]
        public void Move_file_does_not_exist()
        {
            var source = @"d:\dev\moove\test-files\source\some1.txt";
            var destination = @"d:\dev\moove\test-files\target\";
            var result = subject.Process(FileSystemOperations.Move, source, destination);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsFalse(result.Item1);
        }
        [TestMethod]
        public void Move_file_blank_path()
        {
            var source = @"";
            var destination = @"d:\dev\moove\test-files\target\";
            var result = subject.Process(FileSystemOperations.Move, source, destination);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsFalse(result.Item1);
        }
        [TestMethod]
        public void Move_dir()
        {
            var subject = new FileOperationsHandler();
            // ensure the dir is there to be moved
            var source = @"d:\dev\moove\test-files\source\dir-to-move\";
            if (!Directory.Exists(source)) {
                subject.PerformDirectoryAction(FileSystemOperations.Copy, @"d:\dev\moove\test-files\setup\moveme\", source);
            }            
            var target = @"D:\Dev\Moove\Test-files\target\dir-to-move";
            var result = subject.PerformDirectoryAction(FileSystemOperations.Move, source, target);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue(result.Item1);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsFalse(Directory.Exists(source));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue(Directory.Exists(target));
        }
        [TestMethod]
        public void Move_dir_does_not_exist()
        {
            var subject = new FileOperationsHandler();            
            var source = @"d:\dev\moove\test-files\source\missing-dir";            
            var target = @"D:\Dev\Moove\Test-files\target\dir-to-move";
            var result = subject.PerformDirectoryAction(FileSystemOperations.Move, source, target);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsFalse(result.Item1);
        }
        [TestMethod]
        public void Move_dir_blank_path()
        {
            var subject = new FileOperationsHandler();            
            var source = @"";            
            var target = @"D:\Dev\Moove\Test-files\target\dir-to-move";
            var result = subject.PerformDirectoryAction(FileSystemOperations.Move, source, target);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsFalse(result.Item1);
        }
        [TestMethod]
        public void Copy_file()
        {
            var source = @"d:\dev\moove\test-files\source\some1.txt";
            // ensure the file is there to be copied
            if (!File.Exists(source)) {
                subject.PerformFileAction(FileSystemOperations.Copy, @"d:\dev\moove\test-files\setup\some1.txt", @"d:\dev\moove\test-files\source\");
            }
            var destination = @"d:\dev\moove\test-files\target\";
            var result = subject.Process(FileSystemOperations.Copy, source, destination);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue(result.Item1);
        }
        [TestMethod]
        public void Copy_file_does_not_exist()
        {
            var source = @"d:\dev\moove\test-files\source\some1.txt";
            var destination = @"d:\dev\moove\test-files\target\";
            var result = subject.Process(FileSystemOperations.Copy, source, destination);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsFalse(result.Item1);
        }
        [TestMethod]
        public void Copy_file_blank_path()
        {
            var source = @"";
            var destination = @"d:\dev\moove\test-files\target\";
            var result = subject.Process(FileSystemOperations.Copy, source, destination);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsFalse(result.Item1);
        }
        [Test]
        [TestCase(@"D:\Dev\Moove\Test-files\target\dir-to-move")]
        [TestCase(@"c:\dump\Mooove\")]
        public void Copy_dir(string target)
        {
            var subject = new FileOperationsHandler();
            // ensure the dir is there to be moved
            var source = @"d:\dev\moove\test-files\setup\moveme\";
            if (!Directory.Exists(source)) {
                subject.PerformDirectoryAction(FileSystemOperations.Copy, @"d:\dev\moove\test-files\source\dir-to-move", source);
            }
            var result = subject.PerformDirectoryAction(FileSystemOperations.Copy, source, target);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue(result.Item1);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue(Directory.Exists(source));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue(Directory.Exists(target));
        }
        [TestMethod]
        public void Copy_dir_does_not_exist()
        {
            var subject = new FileOperationsHandler();            
            var source = @"d:\dev\moove\test-files\source\missing-dir";            
            var target = @"D:\Dev\Moove\Test-files\target\dir-to-move";
            var result = subject.PerformDirectoryAction(FileSystemOperations.Copy, source, target);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsFalse(result.Item1);
        }
        [TestMethod]
        public void Copy_dir_blank_path()
        {
            var subject = new FileOperationsHandler();            
            var source = @"";            
            var target = @"D:\Dev\Moove\Test-files\target\dir-to-move";
            var result = subject.PerformDirectoryAction(FileSystemOperations.Copy, source, target);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsFalse(result.Item1);
        }
    }
}
