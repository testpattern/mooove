﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Mooove.Tests
{
    [TestClass]
    public class FavouritesRepositoryTests
    {
        private const string testDataPath = @"D:\dev\Mooove\Mooove.tests\testdata";

        [TestMethod]
        public void File_not_found()
        {
            var subject = new FavouritesRepository("", testDataPath);
            var result = subject.GetAll();
            Assert.IsTrue(result.Count == 1);
        }

        [TestMethod]
        public void File_empty()
        {
            var filename = "empty.txt";
            var subject = new FavouritesRepository(filename, testDataPath);
            var result = subject.GetAll();
            Assert.IsTrue(result.Count == 1);
        }

        [TestMethod]
        public void File_has_empty_final_line()
        {
            var filename = "two-results-empty-final-line.txt";
            var subject = new FavouritesRepository(filename, testDataPath);
            var result = subject.GetAll();
            Assert.IsTrue(result.Count == 2);
        }

        [TestMethod]
        public void File_has_duplicates()
        {
            var filename = "three-results-two-unique.txt";
            var subject = new FavouritesRepository(filename, testDataPath);
            var result = subject.GetAll();
            Assert.IsTrue(result.Count == 2);
        }

        [TestMethod]
        public void New_favourite_new_file()
        {
            var subject = new FavouritesRepository($"{Guid.NewGuid().ToString()}.txt", testDataPath);
            var item = subject.BuildFromPath(@"c:\users\mark.calder\documents");
            var result = subject.AddItem(item);
            Assert.IsTrue(result.Item1);
        }

        [TestMethod]
        public void New_favourite_existing_file()
        {
            var filename = "append.txt";
            var subject = new FavouritesRepository(filename, testDataPath);
            var item = subject.BuildFromPath(Guid.NewGuid().ToString());
            var result = subject.AddItem(item);
            Assert.IsTrue(result.Item1);
        }

        [TestMethod]
        public void New_duplicate_favourite_existing_file()
        {
            var filename = "append.txt";
            var subject = new FavouritesRepository(filename, testDataPath);
            var item = subject.BuildFromPath(@"c:\users\mark.calder\documents");
            var result = subject.AddItem(item);
            Assert.IsFalse(result.Item1);
        }

        [TestMethod]
        public void Update_item_replaces_old_entry()
        {
            var filename = "update-item.txt";
            var subject = new FavouritesRepository(filename, testDataPath);
            var item = subject.BuildFromPath(@"c:\users\mark.calder\documents");
            item.Uses++;
            var result = subject.UpdateItem(item);
            Assert.IsTrue(result.Item1);
        }

    }
}
