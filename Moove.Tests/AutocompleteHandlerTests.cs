﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Linq;
using Mooove;

namespace Moove.Tests
{
    [TestClass]
    public class AutocompleteHandlerTests
    {
        [TestMethod]
        public void GetFiles_gets_files()
        {
            var path = @"C:\users\mark.calder\documents";
            var subject = new AutocompleteHandler();
            var result = subject.GetFiles(path);
            Assert.IsTrue(result.Any(item => item.Contains(".txt")));
        }
    }
}
